const getSum = (str1, str2) => {
  if(str1 === '') str1 += '0';
  if(str2 === '') str2 += '0';
  if(typeof str1 !== 'string' || typeof str2 !== 'string' || 
    Array.from(str1).some(c => isNaN(c)) || Array.from(str2).some(c => isNaN(c))) return false;
  return (Number(str1) + Number(str2)).toString();
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  var commentsCount = 0;
  listOfPosts.forEach(function(post) {
    if('comments' in post)
      post['comments'].forEach(function(person) {
        if(person.author === authorName)
          commentsCount++;
      });
  });
  return 'Post:' + listOfPosts.filter(post => post.author === authorName).length + 
    ',comments:' + commentsCount;
};

const tickets = (people) => {
  var count = {
    '25' : 0,
    '50' : 0,
    '100' : 0
  };
  var res = people.some(function(money) {
    if(money === 25) {
      count['25']++;
      return false;
    }
    if(count['25'] === 0) return true;
    if(money === 50) {
      count['25']--;
      count['50']++;
      return false;
    }
    if(count['50'] === 0 && count['25'] >= 3) {
      count['25'] -= 3;
      return false;
    }
    if(count['50'] >= 1 && count['25'] >= 1) {
      count['50']--;
      count['25']--;
      return false;
    }
    return true;
  });
  return res ? 'NO' : 'YES';
};

module.exports = {getSum, getQuantityPostsByAuthor, tickets};
